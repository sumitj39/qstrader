
  

# QSTrader for Indian Stock Markets

  

  

QSTrader is a backtesting platform intended for Indian stock markets.

  

It uses open source [qstrader](https://github.com/mhallsmoore/qstrader) framework and adds functionality to work on Indian stock markets.

  

It is currently being used by me for my strategies to backtest on the market.

  

  

## Current features

  

### 1. Full fledged backtester by QSTrader

  

Includes a full fledged backtester [qstrader](https://github.com/mhallsmoore/qstrader) and all its functionalities as is.

  

  

### 2. Download daily closing prices of equities from NSE/BSE.

  

It makes use of Zerodha to download equity prices (Open, High, Low, Close, Volume) for intraday and closing prices.

  

  

### 3. Notification system

  

By installing it in cloud/RaspberryPi/PC and running it as cron job, users can get daily notifications.

  

## Upcoming features

  

### 1. Live streaming using Websockets

  

The idea is to implement live streaming during market hours from Zerodha, using Zerodha's state of the art Kite-Connect APIs.

  

  

### 2. Better notification system

  

The idea is to integrate cron jobs, telegram APIs to make easier and flexible notification system.

  
  

## Installation

  

  

### 1. Clone the project

  

  

```bash

git clone https://bitbucket.org/sumitj39/qstrader.git

cd qstrader # go into bhavcopy directory

```

  

### 2. Create a virtual environment & install dependencies

  

(read more about creating virtual enviroments [here](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/))

  

```bash

python3 -m venv venv

source venv/bin/activate # activate the virtual environment

pip install -r requirements.txt

```

  

### 4. Install qstrader locally.

Instead of using qstrader as a python pip package, we install it locally so that we can push changes to current repository easily.

  
```bash

python setup.py install

```

### 5. Download sample data

ZerodhaConnector. py can be used to download data in ```qstrader/data/\<duration\>``` directory. Where ```duration``` refers to day, minute, 15minute, etc.
Eg. To dowload daily pricing data for ```HDFC``` from ```2010-01-01``` to ```2019-01-01``` run below command

```bash
python ZerodhaConnector.py -i 'HDFC' -s '2010-01-01' -e '2019-10-01'
```
This will be stored as ```qstrader/data/day/HDFC.csv```
More on this below:
```bash
# use python ZerodhaConnector.py --help to see how to use this utility

# python ZerodhaConnector.py --help
# Usage: ZerodhaConnector.py [OPTIONS]
# Options:
# -i, --instrument TEXT instrument name, eg: INFY [required]
# -s, --start TEXT start date in YYYY-mm-dd format, eg: 2010-10-23 [required]
# -e, --end TEXT end date in YYYY-mm-dd format, eg: 2019-10-23  [required]
# -d, --duration TEXT duration from 
# [minute,3minute,5minute,10minute,15minute,
# 30minute,60minute,day], eg: 3minute [default: day]
# -x, --exchange TEXT exchange name, eg: BSE [default:NSE]
# --help Show this message and exit.

# to download daily data for M&M
python ZerodhaConnector.py -i 'M&M' -s '2019-01-01' -e '2019-10-01' 
# to download 1 minute data for INFY from BSE
python ZerodhaConnector.py -i 'INFY' -s '2019-01-01' -e '2019-10-01' -d minute -x BSE
```
  
## Usage

## Issues

For issues related to installation and usage, please mail me at [sumit_jogalekar@outlook.com](mailto:sumit_jogalekar@outlook.com)