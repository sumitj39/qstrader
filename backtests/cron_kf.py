import ZerodhaUpdater
import pandas as pd
import constants
import numpy as np
import kalman_qstrader_backtest as kalman_backtest
from qstrader import settings
import datetime
 #*/1 * * * * cd $HOME/qstrader/ && source qstrader3/bin/activate && python examples/telegram_bot.py

ticker_x = ['HDFCBANK', 'GOLDBEES', 'IRFC-N9']
ticker_y = ['HDFC', 'KOTAKGOLD', 'IRFC-N1']
entry_z = list(map(lambda x:round(x,3), [0.4342857142857143, 0.01, 0.7171428571428572]))

#entry_z = [0.4342857142857143, 0.01, 0.7171428571428572]
exit_z = [0.01, 0.01, 0.01]
kf_inputs_file = "KF_INPUTS.csv"
suggestions_file = "suggestions_{0}.csv".format(datetime.datetime.today().strftime("%d%m%Y"))

"""
entry_z and exit_z are found by running grid_search.py file.
Currently, the KF_INPUTS.csv needs to be updated manually for best kalman entry and exit points.
Future scope would be to integrate grid_search and cron_kf.
Since we won't be updating the entry and exit points every day, it makes sense to manually update it
only once.
"""
def update_table():
    if len(ticker_x) != len(ticker_y):
        raise ValueError("len(ticker_x) != len(ticker_y)")
    df = pd.DataFrame({'ticker_x': ticker_x, 'ticker_y':ticker_y, 'entry_z': entry_z, 'exit_z': exit_z})
    df.to_csv(constants.get_csv_dir()+kf_inputs_file)


def read_table():
    df = pd.read_csv(constants.get_csv_dir()+kf_inputs_file, index_col=0)
    return df


def kalman_on_one(xticker, yticker, entry_z, exit_z):
    #Fisrt call zerodha_updater and get updated csv.
    exchange = 'NSE'
    ZerodhaUpdater.zerodha_updater(xticker, exchange)
    ZerodhaUpdater.zerodha_updater(yticker, exchange)
    config = settings.from_file(settings.DEFAULT_CONFIG_FILENAME, False)
    results = kalman_backtest.run(config=config, testing=False, tickers=(xticker, yticker), filename='',
                                start='2010-01-01', end=(datetime.datetime.today().date() + datetime.timedelta(1)).strftime("%Y-%m-%d"),
                                entry_z=entry_z, exit_z=exit_z, plot_fig=False)
    tradelogdf = pd.read_csv(constants.get_out_dir()+"suggestion_"+datetime.datetime.today().strftime("%Y-%m-%d")+".csv",
                             parse_dates=True, index_col=0)
    # todaylog = tradelogdf[tradelogdf.index == datetime.datetime.today().strftime("%Y-%m-%d")]
    todaylog = tradelogdf[tradelogdf.index == tradelogdf.index[-1]]

    #TODO
    """
        1. Check for empty df.
        2. Integrate it with telegram api
    """
    print(todaylog)


def kalman_on_many(table):
    for i in range(table.shape[0]):
        pair = table.iloc[i].loc[['ticker_x','ticker_y', 'entry_z', 'exit_z']]
        xticker, yticker = pair['ticker_x'], pair['ticker_y']
        entry_z, exit_z = pair['entry_z'], pair['exit_z']
        kalman_on_one(xticker, yticker,entry_z, exit_z)

if __name__ == "__main__":
    update_table()
    kalman_on_many(read_table())
