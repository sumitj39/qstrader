import telegram
import time
import os
import datetime
import re
import subprocess

TOKEN = '831391001:AAHQpJ_N0u6N8XMBqIgCQWWQXm1ONRx0TPE' # GET THIS FROM TELEGRAM 
bot = telegram.Bot(token=TOKEN)
chat_id = 169728777 # YOUR CHAT ID PRAJWAL'S CHAT ID 

def parse_suggestions(sugg_file):
    f = open(sugg_file).read().split("\n")
    textlist = []
    for line in f:
        if re.search("long|short|closing", line, re.I):
            textlist.append(line)
    return "\n\n".join(textlist)

def good_morning(chat_id=169728777):
    sugg_file = "suggestions_{0}".format(datetime.datetime.today().strftime("%Y%m%d"))
    os.system("python cron_kf.py > {0}".format(sugg_file))
    text = parse_suggestions(sugg_file)
    bot.send_message(chat_id=chat_id, text="origin: " + subprocess.check_output('hostname').decode()+"\nSuggestions on {0} are:\n\n".format(datetime.datetime.today().strftime("%d-%m-%Y"))+text)
    print('Sent message')

# def send_desidime_alerts():
#     message = ''
#     for i in range(1, 3):
#         string = 'https://www.desidime.com/forums/hot-deals-online?page=' + str(i)
#         r = requests.get(string)
#         soup = BeautifulSoup(r.content, 'html.parser')
#         result = soup.find_all(href=re.compile('forums/hot-deals-online/topics'))
#         for i in result[1:]:
#             hotness = i.parent.previous_sibling.previous_sibling
#             if hotness:
#                 message += i.string.replace('\n', ' ') + '\n'
#                 message += 'https://www.desidime.com' + i["href"] + '\n\n'
#     bot.send_message(chat_id=chat_id, text=message)

# schedule.every().day.at("18:08").do(good_morning)
# schedule.every(2).hours.do(send_desidime_alerts)
# schedule.every(10).seconds.do(send_desidime_alerts)

good_morning()
# while False:
#     schedule.run_pending()
#     time.sleep(1)
