import numpy as np
from qstrader import settings
import kalman_qstrader_backtest as kalman_backtest
import qstrader.statistics.performance as perf
import matplotlib.pyplot as plt
import sys


def grid_search():
    grid = []
    enz_start, enz_end, enz_n = 0.01,1.0, 2
    exz_start, exz_end, exz_n = 0.01, 1.0, 2
    for enz in np.linspace(enz_start, enz_end, enz_n):
        for exz in np.linspace(exz_start, exz_end, exz_n):
            if enz >= exz:
                grid.append((enz, exz))
    return grid


def kalman_grid(xticker, yticker):
    tickers = [xticker, yticker]
    config = settings.from_file(settings.DEFAULT_CONFIG_FILENAME, False)

    grid = grid_search()
    #override for test
    #grid = [(0.3,0.1), (1.1, 0.6)]
    cum_returns_list = [] # returns
    # tot_ret = cum_returns[-1] - 1.0

    sharpe_list = [] #results["sharpe"] #results["max_drawdown_pct"] * 100.0
    max_dd_pct_list = []
    cagr_list = []
    for (entry_z, exit_z) in grid:
        results = kalman_backtest.run(config=config, testing=False, tickers=tickers, filename='',
                                      start='2010-01-01', end='2019-07-07',
                                      entry_z=entry_z, exit_z=exit_z, plot_fig=False)
        cum_returns = results['cum_returns']
        total_ret = cum_returns[-1] - 1.0
        cum_returns_list.append(total_ret*100)
        sharpe_list.append(results['sharpe'])
        max_dd_pct_list.append(results['max_drawdown_pct']*100.0)
        cagr = perf.create_cagr(cum_returns)
        cagr_list.append(cagr*100)
    
    str_grid = [",".join(str(x) for x in g) for g in grid]
    print(str_grid)
    print(grid)

    best = cum_returns_list.index(max(cum_returns_list))
    print(best)
    print("Best total returns: {0}, occurs for {1}".format(cum_returns_list[best], grid[best]))

    best = sharpe_list.index(max(sharpe_list))
    print(best)
    print("Best Sharpe: {0}, occurs for {1}".format(sharpe_list[best],  grid[best]))

    best = max_dd_pct_list.index(min(max_dd_pct_list))
    print(best)
    print("Best DD(least dd): {0}, occurs for {1}".format(max_dd_pct_list[best],  grid[best]))

    best = cagr_list.index(max(cagr_list))
    print(best)
    print("Best CAGR: {0}, occurs for {1}".format(cagr_list[best],  grid[best]))

    # print(cum_returns_list)
    # print(sharpe_list)
    # print(max_dd_pct_list)
    # print(cagr_list)
    plt.close()
    plt.cla()
    plt.clf()
    fig = plt.figure(figsize=(12,9))
    ax1 = fig.add_subplot(221)
    ax2 = fig.add_subplot(222)
    ax3 = fig.add_subplot(223)
    ax4 = fig.add_subplot(224)
    #plt.subplot('211')

    # ax1.scatter(x=range(len(cum_returns_list)), y=cum_returns_list)
    ax1.plot(cum_returns_list)

    ax1.set_xticks(np.arange(len(str_grid)), str_grid)
    ax1.set_title('cumulative returns')

    #plt.subplot('212')
    # ax2.plot(x=range(len(sharpe_list)), y=sharpe_list)
    ax2.plot(sharpe_list)

    ax2.set_xticks(np.arange(len(str_grid)), str_grid)
    ax2.set_title('sharpe')

    # ax3.plot(x=range(len(max_dd_pct_list)), y=max_dd_pct_list)
    ax3.plot(max_dd_pct_list)

    ax3.set_xticks(np.arange(len(str_grid)), str_grid)
    ax3.set_title('Max Drawdown pct')

    # ax4.plot(x=range(len(cagr_list)), y=cagr_list)
    ax4.plot(cagr_list)

    ax4.set_xticks(np.arange(len(str_grid)), str_grid)
    ax4.set_title('CAGR')
    fig.show()
    input("something to wait on")

if __name__ == "__main__":
    xtick,ytick = sys.argv[1:]
    print("grid search on x={0},y={1}".format(xtick, ytick))
    kalman_grid(xtick, ytick) #'IRFC-N9','IRFC-N1')
