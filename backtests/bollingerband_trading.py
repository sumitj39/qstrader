from cadf_test import cadf_test, get_pricing
import matplotlib.pyplot as plt
import pandas as pd


def bollingerband(tseries):
    window = 25
    entry_zscore = 1.5
    exit_zscore = 0.5
    invested = ""
    for i in range(len(tseries)-window):
        w = tseries[i:i+window]
        zscores = (w-w.mean())/w.std()
        for zscore in zscores:
            if zscore < -entry_zscore and ("" ==invested):
                print("Buy")
                invested = "BOT"
            if zscore > -exit_zscore and ("BOT"==invested):
                print("Exit the Buy")
                invested = ""

            if zscore > entry_zscore and ("" == invested):
                print("Short")
                invested = "SLD"
            if zscore < exit_zscore and ("SLD" == invested):
                print("Exit the Short")
                invested = ""

if __name__ == '__main__':
    ret = cadf_test("HDFC", "HDFCBANK")
    plt.plot(ret['residual'])
    plt.show()
    bollingerband(ret['residual'])