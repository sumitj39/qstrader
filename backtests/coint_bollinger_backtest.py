# coint_bollinger_backtest.py

import datetime
import ZerodhaUpdater
import click
import numpy as np

from qstrader import settings
from qstrader.compat import queue
from qstrader.price_parser import PriceParser
from qstrader.price_handler.yahoo_daily_csv_bar import YahooDailyCsvBarPriceHandler
from qstrader.strategy.base import Strategies
from qstrader.position_sizer.naive import NaivePositionSizer
from qstrader.risk_manager.example import ExampleRiskManager
from qstrader.portfolio_handler import PortfolioHandler
from qstrader.compliance.example import ExampleCompliance
from qstrader.execution_handler.ib_simulated import IBSimulatedExecutionHandler
from qstrader.statistics.tearsheet import TearsheetStatistics
from qstrader.trading_session import TradingSession
import matplotlib.pyplot as plt
from coint_bollinger_strategy import CointegrationBollingerBandsStrategy


def run(config, testing, tickers, filename,start, end):

    # Set up variables needed for backtest
    events_queue = queue.Queue()
    csv_dir = config.CSV_DATA_DIR
    initial_equity = 100000.00

    # Use Yahoo Daily Price Handler
    start_date = datetime.datetime.strptime(start, "%Y-%m-%d")
    end_date = datetime.datetime.strptime(end, "%Y-%m-%d")

    # Use the Cointegration Bollinger Bands trading strategy
    weights = np.array([1.0,0.7837])
    lookback = 25
    entry_z = 1.5
    exit_z = 0.5
    base_quantity = 10
    strategy = CointegrationBollingerBandsStrategy(
        tickers, events_queue,
        lookback, weights,
        entry_z, exit_z, base_quantity
    )
    strategy = Strategies(strategy)
    # Use the Tearsheet Statistics
    title = ["Cointegration bollinger band strategy - %s/%s"%(tickers[0],tickers[1])]

    # Set up the backtest
    backtest = TradingSession(config, strategy, tickers, initial_equity,start_date, end_date, events_queue, title=title)

    results = backtest.start_trading(testing=testing)
    plt.show()
    return results


@click.command()
@click.option('--config', default=settings.DEFAULT_CONFIG_FILENAME, help='Config filename')
@click.option('--testing/--no-testing', default=False, help='Enable testing mode')
@click.option('--tickers', default='SPY', help='Tickers (use comma)')
@click.option('--filename', default='', help='Pickle (.pkl) statistics filename')
@click.option('--start', default='2015-01-01', help="Start date for backtester")
@click.option("--end", default="today", help="End date for backtester")
def main(config, testing, tickers, filename, start, end):
    tickers = tickers.split(",")
    if end == 'today':
        end = datetime.datetime.today().strftime("%Y-%m-%d")
        for ticker in tickers:
            break
            ZerodhaUpdater.main(ticker, "NSE")
    config = settings.from_file(config, testing)
    run(config, testing, tickers, filename, start, end)


if __name__ == "__main__":
    main()
