import datetime
import pickle
from collections import deque

import matplotlib.pyplot as plt
import numpy as np

from qstrader import settings
from qstrader.compat import queue
from qstrader.event import SignalEvent, EventType
from qstrader.price_handler.yahoo_daily_csv_bar import YahooDailyCsvBarPriceHandler
from qstrader.risk_manager.hidden_markov_model_risk_manager import HiddenMarkovModelRiskModel
from qstrader.strategy.base import AbstractStrategy
from qstrader.trading_session import TradingSession


class MovingAverageCrossStrategy(AbstractStrategy):
    """
    Requires:
    ticker - The ticker symbol being used for moving averages
    events_queue - A handle to the system events queue
    short_window - Lookback period for short moving average
    long_window - Lookback period for long moving average
    """
    def __init__(
        self, ticker,
        events_queue,
        short_window=100,
        long_window=300,
        base_quantity=100
    ):
        self.ticker = ticker
        self.events_queue = events_queue
        self.short_window = short_window
        self.long_window = long_window
        self.base_quantity = base_quantity
        self.bars = 0
        self.invested = False
        self.sw_bars = deque(maxlen=self.short_window)
        self.lw_bars = deque(maxlen=self.long_window)

    def calculate_signals(self, event):
        if (
            event.type == EventType.BAR and
            event.ticker == self.ticker
        ):
            # Add latest adjusted closing price to the
            # short and long window bars
            self.lw_bars.append(event.adj_close_price)
            if self.bars > self.long_window - self.short_window:
                self.sw_bars.append(event.adj_close_price)

            # Enough bars are present for trading
            if self.bars > self.long_window:
                # Calculate the simple moving averages
                short_sma = np.mean(self.sw_bars)
                long_sma = np.mean(self.lw_bars)
                # Trading signals based on moving average cross
                if short_sma > long_sma and not self.invested:
                    print("LONG %s: %s" % (self.ticker, event.time))
                    signal = SignalEvent(
                        self.ticker, "BOT",
                        suggested_quantity=self.base_quantity
                    )
                    self.events_queue.put(signal)
                    self.invested = True
                elif short_sma < long_sma and self.invested:
                    print("SHORT %s: %s" % (self.ticker, event.time))
                    signal = SignalEvent(
                        self.ticker, "SLD",
                        suggested_quantity=self.base_quantity
                    )
                    self.events_queue.put(signal)
                    self.invested = False
            self.bars += 1


def run(config, testing, tickers, filename):
    # Backtest information
    title = ['Moving Average Crossover Example on HDFC: 100x300']
    initial_equity = 100000.0
    start_date = datetime.datetime(2015, 1, 1)
    end_date = datetime.datetime(2019, 7, 26)

    # Use the MAC Strategy
    events_queue = queue.Queue()
    strategy = MovingAverageCrossStrategy(
        tickers[0], events_queue,
        short_window=20,
        long_window=50
    )
    pickle_path = "/Users/sujogale/qstrader/out/hmm_model_spy.pkl"
    hmm_model = pickle.load(open(pickle_path, "rb"))
    risk_manager = HiddenMarkovModelRiskModel(hmm_model)
    # risk_manager = ExampleRiskManager()
    # Set up the backtest
    price_handler = YahooDailyCsvBarPriceHandler(
        config.CSV_DATA_DIR, events_queue,
        tickers, start_date=start_date,
        end_date=end_date, calc_adj_returns=True
    )
    backtest = TradingSession(
        config, strategy, tickers,
        initial_equity, start_date, end_date,
        events_queue, title=title,
        benchmark=tickers[1],
        price_handler=price_handler,
        risk_manager=risk_manager
    )
    results = backtest.start_trading(testing=testing)
    plt.show()
    return results


if __name__ == "__main__":
    # Configuration data
    testing = False
    config = settings.from_file(
        settings.DEFAULT_CONFIG_FILENAME, testing
    )
    tickers = ["M&M", "NIFTY 50"]
    filename = None
    run(config, testing, tickers, filename)
