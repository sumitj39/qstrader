# kalman_qstrader_backtest.py

import click

from qstrader import settings
from qstrader.compat import queue
from qstrader.price_parser import PriceParser
from qstrader.price_handler.yahoo_daily_csv_bar import YahooDailyCsvBarPriceHandler
from qstrader.position_sizer.naive import NaivePositionSizer
from qstrader.risk_manager.example import ExampleRiskManager
from qstrader.portfolio_handler import PortfolioHandler
from qstrader.compliance.example import ExampleCompliance
from qstrader.execution_handler.ib_simulated import IBSimulatedExecutionHandler
from qstrader.statistics.tearsheet import TearsheetStatistics
from qstrader.trading_session import TradingSession
import datetime
from kalman_qstrader_strategy import KalmanPairsTradingStrategy
import matplotlib.pyplot as plt

def run(config, testing, tickers, filename, start, end, entry_z=0.4, exit_z=0.1, plot_fig=True):

    # Set up variables needed for backtest
    events_queue = queue.Queue()
    csv_dir = config.CSV_DATA_DIR
    initial_equity = PriceParser.parse(100000.00)

    # Use Yahoo Daily Price Handler
    price_handler = YahooDailyCsvBarPriceHandler(
        csv_dir, events_queue, tickers
    )

    # Use the KalmanPairsTrading Strategy
    strategy = KalmanPairsTradingStrategy(tickers, events_queue, entry_z=entry_z, exit_z=exit_z)

    start_date = datetime.datetime.strptime(start, "%Y-%m-%d")
    end_date = datetime.datetime.strptime(end, "%Y-%m-%d")

    backtest = TradingSession(config, strategy, tickers, initial_equity,start_date, end_date, events_queue, title=["Kalman Filter Strategy"])

    results = backtest.start_trading(testing=testing, plot_fig=plot_fig)
    plt.show()
    # statistics.save(filename)
    return results


@click.command()
@click.option('--config', default=settings.DEFAULT_CONFIG_FILENAME, help='Config filename')
@click.option('--testing/--no-testing', default=False, help='Enable testing mode')
@click.option('--tickers', default='SP500TR', help='Tickers (use comma)')
@click.option('--filename', default='', help='Pickle (.pkl) statistics filename')
@click.option('--start', default='2017-01-01', help="Start date for backtester")
@click.option("--end", default="today", help="End date for backtester")
def main(config, testing, tickers, filename, start, end):
    if end == 'today':
        end = datetime.datetime.today().strftime("%Y-%m-%d")
    tickers = tickers.split(",")
    config = settings.from_file(config, testing)
    run(config, testing, tickers, filename, start, end, entry_z=0.7171428571428572, exit_z=0.01, plot_fig=True)

if __name__ == "__main__":
    main()
