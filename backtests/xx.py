from io import StringIO
import sys

backup = sys.stdout
sys.stdout = StringIO()
print("Hello", end="")
out = sys.stdout.getvalue()
sys.stdout.close()
sys.stdout = backup

print(out.upper())
