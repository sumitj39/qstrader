from qstrader import settings
from qstrader.compat import queue
from qstrader.price_parser import PriceParser
from qstrader.price_handler.yahoo_daily_csv_bar import YahooDailyCsvBarPriceHandler
from qstrader.trading_session import TradingSession
import datetime
from kalman_qstrader_strategy import KalmanPairsTradingStrategy


def run(config, testing, tickers, filename):

    # Set up variables needed for backtest
    events_queue = queue.Queue()
    csv_dir = config.CSV_DATA_DIR
    initial_equity = PriceParser.parse(100000.00)

    # Use the KalmanPairsTrading Strategy
    strategy = KalmanPairsTradingStrategy(tickers, events_queue, entry_z=0.5, exit_z=0.)
    # strategy = Strategies(strategy, DisplayStrategy())

    # # Use the Naive Position Sizer (suggested quantities are followed)
    # position_sizer = NaivePositionSizer()
    #
    # # Use an example Risk Manager
    # risk_manager = ExampleRiskManager()
    #
    # # Use the default Portfolio Handler
    # portfolio_handler = PortfolioHandler(
    #     initial_equity, events_queue, price_handler,
    #     position_sizer, risk_manager
    # )

    # # Use the ExampleCompliance component
    # compliance = ExampleCompliance(config)
    #
    # # Use a simulated IB Execution Handler
    # execution_handler = IBSimulatedExecutionHandler(
    #     events_queue, price_handler, compliance
    # )
    #
    # # Use the default Statistics
    # statistics = TearsheetStatistics(
    #     config, portfolio_handler, title=""
    # )

    # Set up the backtest
    # backtest = Backtest(
    #     price_handler, strategy,
    #     portfolio_handler, execution_handler,
    #     position_sizer, risk_manager,
    #     statistics, initial_equity
    # )
    start_date = datetime.datetime(2010, 2, 1)
    end_date = datetime.datetime(2019, 6, 22)
    backtest = TradingSession(config, strategy, tickers, initial_equity,start_date, end_date, events_queue, title=["Kalman Filter Strategy"])

    results = backtest.start_trading(testing=testing)
    # statistics.save(filename)
    return results


def main():
    tickers = ['EWA', 'EWC']
    config = settings.from_file(settings.DEFAULT_CONFIG_FILENAME, False)
    run(config, False, tickers, '')

