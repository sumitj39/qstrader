#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  6 19:51:01 2019

@author: sujogale
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from statsmodels.graphics.tsaplots import plot_pacf
from statsmodels.tsa.ar_model import AR
from sklearn.metrics import mean_squared_error
from collections import deque
import os

print(os.getcwd())
df = pd.read_csv("../data/INFY.csv", index_col='Date')
cl = pd.DataFrame(df['Close'])
print(cl.tail())
print(type(cl))
cl['logClose'] = np.log(cl['Close'])
cl['diffLogClose'] = cl['logClose'] - cl['logClose'].shift(1)
cl['pctChange'] = cl['Close'].pct_change()
cl = cl.dropna()
print(cl.tail())


plt.figure(1)
# #plot log returns
# plt.subplot("310")
# cl['logClose'].plot()
# #plot diff of log returns
# plt.subplot("311")
# cl['diffLogClose'].plot()

#plot price series itself
# plt.subplot("312")
cl['Close'].plot()
plt.figure(2)
cl['diffLogClose'].plot()


#pacf of prices
plt.figure(3)
plot_pacf(cl['diffLogClose'], lags=50)
plt.show()

def without_feedback(values):

    split=25
    train, test = values[:-split], values[-split:] 
    print(len(values), len(train), len(test))
    #Fit to an AR model
    model = AR(train)
    model_fit = model.fit(ic='bic')
    window = model_fit.k_ar
    coef = model_fit.params
    print(window)
    print(coef)
    
    predict = model_fit.predict(start=len(train), end=len(train)+len(test)-1, dynamic=False)
    
    for i in range(len(predict)):
        print('predicted=%f, expected=%f' % (predict[i], test[i]))
    error = mean_squared_error(test, predict)
    print('Test MSE: %.3f' % error)
    plt.plot(test)
    plt.plot(predict, color='red')
    plt.show()

def with_feedback(values):
    split=25
    train, test = values[:-split], values[-split:]
    model = AR(train)
    model_fit = model.fit(ic='bic')
    k = model_fit.k_ar
    coef = model_fit.params
    print(coef, len(coef))
    window = deque(train[-k:], maxlen=k)
    print(len(window))
    predictions = list()
    for i in range(split):
        window.reverse()
        val = coef[0]
        for j in range(k):
            val += coef[j+1]*window[j]
        predictions.append(val)
        window.reverse()
        window.append(test[i])
    plt.plot(test)
    plt.plot(predictions, color='red')
    plt.show()
    error = mean_squared_error(test, predictions)
    print("MSE for with_feedback: %.3f"%error)
        


def generate_ar1(n=1000, rho1=0.7, rho2=0.3):
    x = np.random.randn(n)
    error = np.random.randn(n)
    for i in range(2,n):
        x[i] = rho1*x[i-1] + rho2*x[i-2] + error[i]
    return x

plt.figure(4)
values = cl['Close'].values

without_feedback(values)

plt.figure(5)
with_feedback(values)

plt.figure(6)
with_feedback(generate_ar1())

plt.figure(7)
without_feedback(generate_ar1())

plt.figure(8)
pd.DataFrame(generate_ar1()).plot()



