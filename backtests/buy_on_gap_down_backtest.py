from buy_on_gap_down_strategy import BuyOnGapDownStrategy
from qstrader import settings
from qstrader.trading_session import TradingSession
import datetime
import queue

def run(config, tickers, testing=False):


    events_queue = queue.Queue()
    strategy = BuyOnGapDownStrategy(tickers, events_queue, zscore=0.5)

    start_date = datetime.datetime(2010,1,1).date()
    end_date = datetime.datetime(2019,5,1).date()

    initial_equity = 10000.0
    title = ['Buy On gap on %s' % ','.join(tickers)]
    session = TradingSession(config, strategy, tickers, start_date=start_date, end_date=end_date, equity=initial_equity, events_queue=events_queue, title=title)
    session.start_trading(testing=testing)
    print(strategy.profits, strategy.losses)
    print(strategy.final_porl)

if __name__ == "__main__":
    testing = False
    config = settings.from_file(
        settings.DEFAULT_CONFIG_FILENAME, testing
    )
    tickers = ['ABB', 'ACC', 'ADANIPORTS', 'AMBUJACEM', 'ASHOKLEY', 'ASIANPAINT',
               'AUROPHARMA',  'AXISBANK', 'BAJAJ-AUTO', 'BAJFINANCE', 'BAJAJFINSV',
               'BAJAJHLDNG',  'BANKBARODA', 'BHEL', 'BPCL', 'BHARTIARTL',
               'BIOCON', 'BOSCHLTD', 'BRITANNIA', 'CADILAHC', 'CIPLA',
               'COALINDIA', 'COLPAL', 'CONCOR', 'DLF', 'DABUR', 'DIVISLAB', 'DRREDDY',
               'EICHERMOT', 'GAIL',  'GODREJCP', 'GRASIM', 'HCLTECH',
               'HDFCBANK', 'HAVELLS', 'HEROMOTOCO', 'HINDALCO', 'HINDPETRO',
               'HINDUNILVR', 'HINDZINC', 'HDFC', 'ICICIBANK', 'ITC',
                'IOC', 'INDUSINDBK', 'INFY',  'JSWSTEEL', 'KOTAKBANK',
               'LT', 'LUPIN', 'MRF', 'M&M', 'MARICO', 'MARUTI', 'MOTHERSUMI', 'NHPC',
               'NMDC', 'NTPC', 'ONGC', 'OFSS', 'PAGEIND', 'PETRONET', 'PIDILITIND', 'PEL',
               'PGHH', 'RELIANCE', 'SHREECEM', 'SRTRANSFIN', 'SIEMENS',
               'SBIN', 'SAIL', 'SUNPHARMA', 'TCS', 'TATAMTRDVR', 'TATAMOTORS', 'TATASTEEL',
               'TECHM', 'TITAN', 'UPL', 'ULTRACEMCO', 'UBL', 'MCDOWELL-N', 'VEDL',
               'IDEA', 'WIPRO', 'YESBANK', 'ZEEL']
    #tickers = ["RCOM",  "NIFTY IT", "KOTAKGOLD", "KOTAKNIFTY",  "HDFC", "HDFCBANK", "INFY", "ADANIPORTS" ,"RELIANCE", "RELINFRA", "TECHM", "TCS", "VEDL", "YESBANK"]
    run(config, tickers, testing)
