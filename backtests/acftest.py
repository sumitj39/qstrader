#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 21 12:28:51 2019

@author: sujogale
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns


x = np.arange(1,100)
y = np.sin(np.arange(1,100))

df = sns.load_dataset("iris")
print(df.head())

sns.pairplot(pd.DataFrame(df[['sepal_length', 'sepal_width', 'species']]), hue = 'species')
#df[['sepal_length', 'sepal_width']].hist()