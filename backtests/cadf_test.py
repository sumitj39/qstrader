import numpy as np
import pandas as pd
import statsmodels.api as sm
import matplotlib.pyplot as plt
from statsmodels.tsa.stattools import adfuller
import sys

def ols(y,x):
    y = np.array(y)
    x = sm.add_constant(x)
    result = sm.OLS(y,x).fit()
    return result

def get_pricing(ticker):
    return pd.read_csv("~/qstrader/data/%s.csv"%ticker, parse_dates=True, index_col=['Date'])

def cadf_test(ticker1, ticker2):
    print(ticker1, ticker2)
    stk1 = get_pricing(ticker1)['Close']
    stk2 = get_pricing(ticker2)['Close']

    # select only intersection entries
    intersect = pd.merge(stk1,stk2, left_index=True, right_index=True)
    stk1, stk2 = intersect['Close_x'], intersect['Close_y']
    print(stk1.shape)
    print(stk2.shape)
    # hdfc = hdfc.loc[hdfc.index > '2010-01-01']
    # hdfc = hdfc.loc[hdfc.index < '2019-01-02']
    # hdfcbank = hdfcbank.loc[hdfcbank.index > '2010-01-01']
    # hdfcbank = hdfcbank.loc[hdfcbank.index < '2019-01-02']
    # plt.scatter(hdfc.values, hdfcbank.values)
    # plt.show()
    stk1,stk2 = stk1.values, stk2.values

    resy1x2 = ols(x=stk2, y = stk1)
    resy2x1 = ols(x=stk1, y=stk2)

    print("intercept, slope for %s as independent :"%ticker1, resy2x1.params[0], resy2x1.params[1])
    print("intercept, slope for %s as independent :"%ticker2, resy1x2.params[0], resy1x2.params[1])

    residy1x2 = stk1 - resy1x2.params[1]*stk2
    residy2x1 = stk2 - resy2x1.params[1]*stk1

    adfresy1x2 = adfuller(residy1x2)
    adfresy2x1 = adfuller(residy2x1)

    print("Test Statistic for %s as independent: "%(ticker1), adfresy2x1[0])
    print("Test Statistic for %s as independent: "%(ticker2), adfresy1x2[0])
    # for key,value in adf_result[4].items():
    #     print(key,value)
    #     if adf_result[0] < value:
    #         print("Test Statistic(%s) < (%s) at %s level"%(adf_result[0], value, key))
    print("%s(y) vs %s(x) "%(ticker2,ticker1) + ("Mean reverting" if adfresy2x1[0] < adfresy2x1[4]['5%'] else "Not mean reverting"))
    print("%s(y) vs %s(x) "%(ticker1,ticker2) + ("Mean reverting" if adfresy1x2[0] < adfresy1x2[4]['5%'] else "Not mean reverting"))
    #print(adfuller(resid))

    plot_resid = False #May be used in future.
    if plot_resid:
        # plt.subplot(211)
        plt.title("%s(y) vs %s(x)"%(ticker2, ticker1))
        plt.plot(residy2x1)
        plt.show()
        # plt.subplot(212)
        plt.title("%s(y) vs %s(x)"%(ticker1, ticker2))
        plt.plot(residy1x2)
        plt.show()
    if adfresy1x2[0] < adfresy2x1[0]:
        return {'meanrev':adfresy1x2[0]<adfresy1x2[4]['5%'],'olsresult':resy1x2,
                'yticker':ticker1, 'xticker':ticker2, 'residual':residy1x2}
    else:
        return {'meanrev':adfresy2x1[0]<adfresy2x1[4]['5%'],'olsresult':resy2x1,
                'yticker':ticker2, 'xticker':ticker1, 'residual':residy2x1}

if __name__ == '__main__':
    res = cadf_test(*sys.argv[1:])
    print(res)
    print(res['olsresult'].params[1])
