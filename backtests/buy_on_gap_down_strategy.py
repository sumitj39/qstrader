from qstrader.strategy.base import AbstractStrategy
from collections import deque
from qstrader.event import EventType
import  numpy as np
from qstrader.price_parser import  PriceParser


class Stock:
    def __init__(self, ticker, lookback_period):
        self.ticker = ticker
        self.close_prices = deque(maxlen=lookback_period)
        self.std = None
        self.prev_low, self.prev_high, self.prev_close, self.prev_open = 0,0,0,0
        self.cur_low, self.cur_high, self.cur_close, self.cur_open = 0, 0, 0, 0
        self.nbars = 0
        self.ma = 0
        self.o2lret = 0

class BuyOnGapDownStrategy(AbstractStrategy):
    # TODO: Support for basket of stocks
    # TODO: Add a moving average filter
    # TODO: Select stocks with least return < -RollingSD
    def __init__(self, tickers, events_queue, lookback_period = 90, zscore=1):
        self.tickers = tickers
        self.events_queue = events_queue
        self.lookback_period = lookback_period + 1

        self.zscore = zscore
        self.stocks = {}
        for ticker in tickers:
            self.stocks[ticker] = Stock(ticker, self.lookback_period)
        self.nBars = 0
        self.profits = 0
        self.losses = 0
        self.final_porl = 0
        self.cur_time = None
        self.latest_events = {}

    def calculate_signals(self, event):
        if (
            event.type == EventType.BAR and
            event.ticker in self.tickers
        ):
            if event.time == self.cur_time:
                self.latest_events[event.ticker] = event
            else:
                self.cur_time = event.time
                for tkr in self.tickers:
                    self.latest_events[tkr] = None
                self.latest_events[event.ticker] = event

            if all(np.array(list(self.latest_events.values())) != None):
                #Get into algorithm only when all prices for that timestamp are obtained.
                screened_stocks = [] # List of screened stocks for buying.
                max_screened_stocks = 10 # max no. of stocks to buy.
                for tkr in self.tickers:
                    stock = self.stocks[tkr]
                    evt = self.latest_events[tkr]
                    #Store previous bar info
                    stock.prev_open, stock.previous_high, stock.previous_low, stock.prev_close = \
                        stock.cur_open, stock.cur_high, stock.cur_low, stock.cur_close
                    # Change cur high and low
                    stock.cur_open, stock.cur_high, stock.cur_low, stock.cur_close = \
                        evt.open_price/PriceParser.PRICE_MULTIPLIER,\
                        evt.high_price/PriceParser.PRICE_MULTIPLIER,\
                        evt.low_price/PriceParser.PRICE_MULTIPLIER,\
                        evt.close_price/PriceParser.PRICE_MULTIPLIER

                    if stock.nbars > self.lookback_period:
                        # We are past 'lookback_period' days. Start trading.
                        clp = np.array(stock.close_prices) # collections.deque doesn't support slice operator
                        # 1.compute moving average.
                        stock.ma = np.average(clp[-20:]) #TODO change 20 to ma_window
                        # 2.Calculate ret from today's open to prev low.
                        stock.o2lret = (stock.cur_open - stock.previous_low) / stock.previous_low
                        # 3.Previous day's close - n-2 day's close in the below formula
                        c2crets = (clp[1:] - clp[:-1]) / clp[:-1]
                        stock.std = c2crets.std()

                        if (stock.o2lret < -1 * self.zscore * stock.std) and (True and stock.cur_open > stock.ma):
                            print("%.5f is less than -%.5f on %s for %s" %
                                  (stock.o2lret, stock.std, event.time, tkr), end="\t")
                            screened_stocks.append(tkr)
                            porl = (stock.cur_close - stock.cur_open)/stock.cur_open
                            self.final_porl += porl
                            if porl > 0:
                                self.profits += 1
                            else:
                                self.losses += 1
                            print("porl in pct %.3f"%(porl*100))

                    stock.close_prices.append(evt.adj_close_price / PriceParser.PRICE_MULTIPLIER)
                    stock.nbars += 1
                if len(screened_stocks) > 0:
                    print(screened_stocks)
"""
            stock = self.stocks[event.ticker]

            stock.prev_open, stock.previous_high, stock.previous_low, stock.prev_close = \
            stock.current_open, stock.current_high, stock.current_low, stock.prev_close

            # Change cur high and low
            stock.current_open, stock.current_high, stock.current_low, stock.prev_close = \
                event.open_price/PriceParser.PRICE_MULTIPLIER,\
                event.high_price/PriceParser.PRICE_MULTIPLIER,\
                event.low_price/PriceParser.PRICE_MULTIPLIER,\
                event.close_price/PriceParser.PRICE_MULTIPLIER

            if stock.nbars >= 20:
                #TODO Change 20 to ma_window
                #To remove data snooping, we will ignore the last close price
                cl = list(stock.close_prices) #deque doesn't support slice operator
                self.ma = np.average(cl[-20:])

            if stock.nbars > stock.lookback_period:
                    
                # We are past 90 days. Start trading.
                # Calculate ret from today's open to prev low.
                stock.o2lret = (stock.current_open - stock.previous_low)/stock.previous_low

                clp = np.array(stock.close_prices)
                # TODO: fix this by writing better code.
                # TODO: Currently, to avoid data snooping, we are counting
                # Previous day's close - n-2 day's close in the below formula
                c2crets = (clp[1:] - clp[:-1])/clp[:-1]
                stock.std = c2crets.std()
                # self.std = np.array(self.close_prices).std()
            
            if allAvailable(self.stocks):
                if (o2lret < -1*self.zscore*self.std) and (True and self.current_open > self.ma):
                    print("%.5f is less than -%.5f on %s" %
                          (o2lret, self.std, event.time))
                    porl = self.close_prices[-1] - self.current_open
                    if porl > 0:
                        self.profits += 1
                    else:
                        self.losses += 1

            stock.close_prices.append(
                event.adj_close_price/PriceParser.PRICE_MULTIPLIER)
            stock.nbars += 1
"""