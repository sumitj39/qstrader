import os
CSV_DIR = "~/qstrader/data/"
OUT_DIR = "~/qstrader/out/"
EXAMPLES_DIR = "~/qstrader/examples/"
def get_csv_dir():
    return os.path.expanduser(CSV_DIR)

def get_out_dir():
    return os.path.expanduser(OUT_DIR)

def get_examples_dir():
    return os.path.expanduser(EXAMPLES_DIR)
