# regime_hmm_train.py

from __future__ import print_function

import datetime
import pickle
import warnings

from hmmlearn.hmm import GaussianHMM
from matplotlib import cm, pyplot as plt
from matplotlib.dates import YearLocator, MonthLocator
import numpy as np
import pandas as pd
import seaborn as sns

def obtain_prices_df(csv_filepath, end_date):
    stock = pd.read_csv(csv_filepath, parse_dates=True, index_col="Date")
    stock['Returns'] = stock['Close'].pct_change()
    return stock[: end_date.strftime("%Y-%m-%d")].dropna()

def plot_in_sample_hidden_states(hmm_model, df,rets):
    """
    Plot the adjusted closing prices masked by
    the in-sample hidden states as a mechanism
    to understand the market regimes.
    """
    # Predict the hidden states array
    hidden_states = hmm_model.predict(rets)
    print(hidden_states[:])
    # Create the correctly formatted plot
    fig, axs = plt.subplots(
        hmm_model.n_components,
        sharex=True, sharey=True
    )
    colours = cm.rainbow(
        np.linspace(0, 1, hmm_model.n_components)
    )
    for i, (ax, colour) in enumerate(zip(axs, colours)):
        mask = hidden_states == i
        ax.plot_date(
            df.index[mask],
            df["Adj Close"][mask],
            ".", linestyle='none',
            c=colour
        )
        ax.set_title("Hidden State #%s" % i)
        ax.xaxis.set_major_locator(YearLocator())
        ax.xaxis.set_minor_locator(MonthLocator())
        ax.grid(True)
    plt.show()

if __name__ == "__main__":
    # Hides deprecation warnings for sklearn
    warnings.filterwarnings("ignore")

    # Create the SPY dataframe from the Yahoo Finance CSV
    # and correctly format the returns for use in the HMM
    csv_filepath = "/Users/sujogale/qstrader/data/M&M.csv"
    pickle_path = "/Users/sujogale/qstrader/out/hmm_model_spy.pkl"
    end_date = datetime.datetime(2014, 12, 31)
    spy = obtain_prices_df(csv_filepath, end_date)
    rets = np.column_stack([spy["Returns"]])
    print(rets.shape)
    # Create the Gaussian Hidden markov Model and fit it
    # to the SPY returns data, outputting a score
    hmm_model = GaussianHMM(
        n_components=2, covariance_type="full", n_iter=1000
    ).fit(rets)
    print("Model Score:", hmm_model.score(rets))

    # Plot the in sample hidden states closing values
    plot_in_sample_hidden_states(hmm_model, spy, rets)

    print("Pickling HMM model...")
    pickle.dump(hmm_model, open(pickle_path, "wb"))
    print("...HMM model pickled.")


    preddf = pd.read_csv(csv_filepath, parse_dates=True, index_col='Date')[end_date.strftime("%Y-%m-%d"): ] #['2015-01-01':]
    preddf['Returns'] = preddf['Close'].pct_change()
    preddf.dropna(inplace=True)
    # preddf = preddf.iloc[-1:]
    print(preddf)
    rets = np.column_stack([preddf['Returns']])
    print(rets.shape)
    hmm_model1 = pickle.load(open(pickle_path, "rb"))
    print(hmm_model1.n_components)
    plot_in_sample_hidden_states(hmm_model1, preddf, rets)
