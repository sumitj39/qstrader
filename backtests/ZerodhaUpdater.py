import pandas as pd
from pathlib import Path
import sys
import os
from ZerodhaConnector import batch_get_day_candles_from_zerodha
import datetime

def get_first_last_date(df):
    if not 'Date' in df.columns:
        print("Date column not found in columns")
        raise TypeError("Date column not found in columns")
    df = df.sort_values(by='Date')
    return df['Date'].iloc[[0,-1]].values

def main(sname, ex):
    CSV_DIR = "~/qstrader/data/"
    stock_csv = CSV_DIR + "%s.csv" % sname
    if Path(os.path.expanduser(stock_csv)).is_file():
        stock = pd.read_csv(stock_csv, parse_dates=True)
        lastdate = get_first_last_date(stock)
        df = batch_get_day_candles_from_zerodha(sname, ex, lastdate[1], datetime.datetime.today().strftime("%Y-%m-%d"))
        if df is not None:
            stock = stock.append(df, ignore_index=True).drop_duplicates("Date").sort_index()
            # stock = stock.drop_duplicates("Date")
            # stock = stock.sort_index()
        stock.to_csv(stock_csv, index=False)

def zerodha_updater(sname, ex):
    main(sname, ex)
if __name__ == '__main__':
    if len(sys.argv) < 3:
        print("not enough arguments")
        print("help: python %s stockname exchangename" % (sys.argv[0]))
        exit(0)
    main(*sys.argv[1:])
