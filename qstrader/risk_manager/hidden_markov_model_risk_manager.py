
from qstrader.risk_manager.base import AbstractRiskManager
from qstrader.event import OrderEvent
import numpy as np

class HiddenMarkovModelRiskModel(AbstractRiskManager):

    def __init__(self, hmm_model):
        #hmm_model = pickle.load(open(pickle_path, "rb"))
        self.hmm_model = hmm_model
        
    def refine_orders(self, portfolio, sized_order):

        hmm_execute_order = self.hmm_allow_order(portfolio.price_handler)

        if hmm_execute_order:
            print("Allowing order")
            order_event = OrderEvent(
            sized_order.ticker,
            sized_order.action,
            sized_order.quantity
            )
            return [order_event]
        else:
            print("Not allowing order")
            return []

    def hmm_allow_order(self,price_handler):
        # TODO Change
        # TODO: Implement the logic for hidden markov model here
        # After that, set hmm_execute_order to True/False based on condition
        adj_close_ret = np.vstack(price_handler.adj_close_returns)
        ret = self.hmm_model.predict(adj_close_ret)[-1]
        print(ret)

        # The idea for comparing twice rather than if.. else is that we may have more
        # than two states for some models.
        # 0-Allow, 1-Don't_allow for M&M from 2010-apr to 2014-dec
        if ret == 0:
            return  True
        if ret == 1:
            return  False
        #By default, disallow
        return False